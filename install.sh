#!/bin/bash

# I would like to thank a few people first
# https://github.com/betterbrowser/arcfox
# https://github.com/gorhill/uBlock

# Imagine if Firefox was a Gnome application from the start. OMFG!
curl -s -o- https://raw.githubusercontent.com/rafaelmardojai/firefox-gnome-theme/master/scripts/install-by-curl.sh | bash

# Lets be honest 95% of the internet is unusable without this.
wget https://addons.mozilla.org/firefox/downloads/file/4261710/ublock_origin-1.57.2.xpi
firefox -install-global-extension ublock_origin-1.57.2.xpi

# It wouldn't be yotsuba browser without this.
sudo mkdir /usr/share/yotsuba-homepage/
cd /usr/share/yotsuba-homepage/
wget https://codeberg.org/contractover/Yotsuba-homepage/releases/download/1.0/Yotsuba-homepage.zip
cd ~/.mozilla/firefox/
sudo touch prefs.js
sudo echo 'user_pref("browser.startup.homepage", "/usr/share/Yotsuba-homepage/HomePage.html");' | tee -a ~/.mozilla/firefox/prefs.js

# No one should ever use Google.
sudo echo 'user_pref("browser.search.defaultenginename", "Startpage");
user_pref("browser.search.defaulturl", "https://www.startpage.com/do/dsearch?query=");' | tee -a ~/.mozilla/firefox/prefs.js


# You may qestion this. But trust me, Some of us really do need to disaible private browsing.
cd /usr/lib/firefox/distribution
sudo touch policies.json
sudo echo "{
  "policies": {
    "DisablePrivateBrowsing": true
  }
}" | tee -a /usr/lib/firefox/distribution/policies.json